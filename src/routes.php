<?php

use Slim\Http\Request;
use Slim\Http\Response;
use \Firebase\JWT\JWT;
use Tuupola\Base62;

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post('/login', function ($request, $response) {
    // check username + password | check session table | update or create new | done
    $input = $request->getParsedBody();
    $passwordHash = md5($input['password']);

    $sql = "SELECT * FROM users_table WHERE LOWER(email) = LOWER('".$input['email']."') AND password = '".$passwordHash."' AND status NOT IN (3)";
    $stmt = $this->database->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll();

    if(empty($result)){
        $data['msg'] = 'your account is no longer active.';
    }else{
        // check credit / update
        // $dt = date();
        $monthNow = date('m');
        $userLastCheck = date("m",strtotime($result[0]['lastCreditCheck']));
        if($monthNow != $userLastCheck){
            print_r('updated');
            User::where('id', $result[0]['id'])->update([
                'credit'            => ($result[0]['status'] == 1 ? 20 : 40),
                'lastCreditCheck'   => date('Y-m-d')
            ]);
        }
        
        $email      = $result[0]['email'];
        $status     = $result[0]['status'];
        $id         = $result[0]['id'];
        $firstName  = $result[0]['firstName'];
        $lastName   = $result[0]['lastName'];
        $credit     = $result[0]['credit'];

        $now = new DateTime();
        // $future = new DateTime("+60 minutes"); +1 day
        $future = new DateTime("+1 day");
        // $server = $request->getServerParams();
        $jti = (new Base62)->encode(random_bytes(16));
        $settings = $this->get('settings'); // get settings array.
        $payload = [
            "iat"           => $now->getTimeStamp(),
            "exp"           => $future->getTimeStamp(),
            "jti"           => $jti,
            "id"            => $id,
            'email'         => $email,
            'status'        => $status,
            'credit'        => $credit,
            'firstName'     => $firstName,
            'lastName'      => $lastName
        ];
        $token = JWT::encode($payload, $settings['jwt']['secret'], "HS256");
        $data["token"] = $token;
        $data["expires"] = $future->getTimeStamp();
        $data["now"] = $now->getTimeStamp();
        $data['status'] = $status;
        $data['email'] = $email;
        $data['id'] = $id;
    }
    return $response->withStatus(200)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

});
