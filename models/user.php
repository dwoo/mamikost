<?php
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Model;

class User extends Model{
    public $timestamps = false;
    protected $table = 'users_table';
    protected $fillable = ['firstName','lastName','email','password','status','credit','lastCreditCheck','createdDate','lastModify'];
}