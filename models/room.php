<?php
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Model;

class Room extends Model{
    public $timestamps = false;
    protected $table = 'kost_table';
    protected $fillable = [ 'ownerid','roomName','address','city','latlong','availableRoom','price','category','roomMeasure','roomFacility','bathRoomFacility','publicFacility','parkingFacility','publicAccess','otherFacility','otherDesc','additionalCost','createdDate','lastModify'];
}