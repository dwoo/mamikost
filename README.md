# Test Backend (Long Version)

----
Technical things
----
* SLIM 3 Framework - `https://www.slimframework.com`
* MYSQL Database
* postman

----
Feature
----
* JWT Auth
* RESTful
* CRUD


## Install the Application

first, make sure you have git, composer and php 7.2 installed :)

* clone the repo `git@gitlab.com:dwoo/mamikost.git` or you can go to `https://gitlab.com/dwoo/mamikost` and download it as zip file.
* on your console, move inside the project folder.
* type `composer install`.
* if you install php as standalone, you can direct run the app with `composer start`, But if you use XAMPP or other apache bundle, make sure you out the project at `htdoc`.

## Live Demo API
https://mamikost.herokuapp.com

## postman collection

https://www.getpostman.com/collections/c7270058b7180159c4c5