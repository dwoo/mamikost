<?php

$app->group('/api', function () use ($app){ 

    $app->get('/user', function ($request, $response) {
        // put log message
        // $this->logger->info("getting all user");
        $data = User::all();
        return $this->response->withJson(["status" => "success", "data" => $data], 200);
    });

    $app->get('/user/[{id}]', function ($request, $response,$args) {
        // put log message
        // $this->logger->info("getting all user");
        $data = User::find($args['id']);
        return $this->response->withJson(["status" => "success", "data" => $data], 200);
    });
    
    $app->post('/user', function ($request, $response) {
        $input  = $request->getParsedBody();
        $data   = User::create([
            'firstName'         => $input['firstName'],
            'lastName'          => $input['lastName'],
            'email'             => $input['email'],
            'password'          => $input['password'],
            'status'            => $input['status'],
            'lastCreditCheck'   => $input['lastCreditCheck'],
            'createdDate'       => $input['createdDate'],
            'lastModify'        => $input['lastModify']
        ]); 
        return $this->response->withJson(["status" => "success"], 200);
    });

    $app->put('/user/[{id}]', function ($request, $response, $args) {
        // put log message
        // $this->logger->info("updating book");
        $input = $request->getParsedBody();
        $data = User::where('id', $args['id'])->update([
            'firstName'         => $input['firstName'],
            'lastName'          => $input['lastName'],
            'email'             => $input['email'],
            'password'          => $input['password'],
            'status'            => $input['status'],
            'lastCreditCheck'   => $input['lastCreditCheck'],
            'createdDate'       => $input['createdDate'],
            'lastModify'        => $input['lastModify']
        ]);
        return $this->response->withJson(["status" => "success","data" => $data], 200);
    });

    $app->delete('/user/[{id}]', function ($request, $response, $args) {
        // put log message
        // $this->logger->info("deleting book");
        $data = User::destroy($args['id']);
        return $this->response->withJson($data, 200);
    });


});