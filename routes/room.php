<?php

$app->group('/api', function () use ($app){ 

    $app->get('/room', function ($request, $response) {
        // put log message
        // $this->logger->info("getting all user");
        $data = Room::all();
        return $this->response->withJson(["status" => "success", "data" => $data], 200);
    });

    $app->get('/room/[{id}]', function ($request, $response,$args) {
        // put log message
        // $this->logger->info("getting all user");
        $data = Room::find($args['id']);
        return $this->response->withJson(["status" => "success", "data" => $data], 200);
    });

    $app->get('/ask_availroom/[{id}]', function ($request, $response, $args){
        $token = $request->getHeader("Authorization");
        $final = str_replace("Bearer ","",$token);
        $extract =  $request->getAttribute('decoded_token_data');

        $sql = "SELECT * FROM users_table WHERE LOWER(email) = LOWER('".$extract['email']."')";
        $stmt = $this->database->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        if($result[0]['credit'] > 0){
            $data = Room::find($args['id']);
            User::where('id', $args['id'])->update([
                'credit'    => ($result[0]['credit'] - 5)
            ]);
            $res['status'] = 'success';
        }else{
            $res['status'] = 'fail';
            $res['msg'] = 'your credit not enough';
        }
        $res['data'] = $data;
        return $response->withStatus(200)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($res, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

    });

    $app->get('/search_room', function ($request, $response, $args) {
        $name   = $request->getQueryParam('name');
        $city   = $request->getQueryParam('city');
        $price  = $request->getQueryParam('price');

        $shortBy = $request->getQueryParam('shortBy');
        $order = $request->getQueryParam('order');

        $query = "SELECT * FROM kost_table WHERE (roomName LIKE :names OR city LIKE :city OR price LIKE :price)";
        if(!empty($request->getQueryParam('shortBy'))){
            $query .= "ORDER BY :shortBy :order";
        }
        $sql = $this->database->prepare($query);
        $queryName  = "%".$name."%";
        $queryCity  = "%".$city."%";
        $queryPrice = "%".$price."%";
        $sql->bindParam("roomName", $queryName);
        $sql->bindParam("city", $queryCity);
        $sql->bindParam("price", $queryPrice);
        $sql->bindParam("shortBy", $shortBy);
        $sql->bindParam("order", ($order == "" ? "ASC" : $order));
        $sql->execute();
        $result = $sql->fetchAll();
        $data = $result;

        return $this->response->withJson(["status" => "success", "data" => $data], 200);
    });
    
    $app->post('/room', function ($request, $response) {
        $input  = $request->getParsedBody();
        $data   = Room::create([
            'ownerid'               => $input['ownerid'],
            'roomName'              => $input['roomName'],
            'address'               => $input['address'],
            'city'                  => $input['city'],
            'latlong'               => $input['latlong'],
            'availableRoom'         => $input['availableRoom'],
            'price'                 => $input['price'],
            'category'              => $input['category'],
            'roomMeasure'           => $input['roomMeasure'],
            'roomFacility'          => $input['roomFacility'],
            'bathRoomFacility'      => $input['bathRoomFacility'],
            'publicFacility'        => $input['publicFacility'],
            'parkingFacility'       => $input['parkingFacility'],
            'publicAccess'          => $input['publicAccess'],
            'otherFacility'         => $input['otherFacility'],
            'otherDesc'             => $input['otherDesc'],
            'additionalCost'        => $input['additionalCost'],
            'createdDate'           => $input['createdDate'],
            'lastModify'            => $input['lastModify']
        ]); 
        return $this->response->withJson(["status" => "success"], 200);
    });

    $app->put('/room/[{id}]', function ($request, $response, $args) {
        // put log message
        // $this->logger->info("updating book");
        $input = $request->getParsedBody();
        $data = Room::where('id', $args['id'])->update([
            'ownerid'               => $input['ownerid'],
            'roomName'              => $input['roomName'],
            'address'               => $input['address'],
            'city'                  => $input['city'],
            'latlong'               => $input['latlong'],
            'availableRoom'         => $input['availableRoom'],
            'price'                 => $input['price'],
            'category'              => $input['category'],
            'roomMeasure'           => $input['roomMeasure'],
            'roomFacility'          => $input['roomFacility'],
            'bathRoomFacility'      => $input['bathRoomFacility'],
            'publicFacility'        => $input['publicFacility'],
            'parkingFacility'       => $input['parkingFacility'],
            'publicAccess'          => $input['publicAccess'],
            'otherFacility'         => $input['otherFacility'],
            'otherDesc'             => $input['otherDesc'],
            'additionalCost'        => $input['additionalCost'],
            'createdDate'           => $input['createdDate'],
            'lastModify'            => $input['lastModify']
        ]);
        return $this->response->withJson(["status" => "success","data" => $data], 200);
    });

    $app->delete('/room/[{id}]', function ($request, $response, $args) {
        // put log message
        // $this->logger->info("deleting book");
        $data = Room::destroy($args['id']);
        return $this->response->withJson($data, 200);
    });



});